package co.id.phenom.repository;

import org.springframework.data.repository.CrudRepository;

import co.id.phenom.entity.User;

public interface UserRepository extends CrudRepository<User, Long> {

}
